# Pick Ticket Reports

Consume events from Kafka, save them to NoSQL store, and make available via REST endpoints

![Pick-Ticket Architecture](pick-ticket.png "Pick-Ticket Architecture")

## Build & Run

### Run Locally (Boot Run)

`./gradlew bootRun`

### Run with Docker

Build the Docker image

`./gradlew buildDocker`

Build and Publish the Docker Image

`./gradlew buildDocker -Ppublish=true`

Run a Docker container

`docker run --rm --network=host --name=pick-ticket-reports registry.gitlab.com/sageburner/webflux/pick-ticket-reports:0.1.0`

## Tools

### Cassandra

`docker run --name cassandra -d -p 7000-7001:7000-7001 -p 7199:7199 -p 9042:9042 -p 9160:9160 cassandra:3.11`