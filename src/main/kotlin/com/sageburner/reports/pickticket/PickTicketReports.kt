package com.sageburner.reports.pickticket

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.PropertySource
import org.springframework.core.env.Environment

/**
 * PickTicketReports application
 *
 * Consume a continuous stream of manual PickTicket requests
 *
 */
@SpringBootApplication
@PropertySource("classpath:application.properties")
class PickTicketReports : InitializingBean, DisposableBean {

    private val log = LoggerFactory.getLogger(PickTicketReports::class.java)

    @Autowired
    private lateinit var env: Environment


    override fun afterPropertiesSet() {
        log.info("Application Version: " + env.getProperty("app.version"))
    }

    override fun destroy() {
        log.info("Destroying ...")
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(PickTicketReports::class.java, *args)
}
