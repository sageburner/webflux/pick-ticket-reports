package com.sageburner.reports.pickticket

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import org.springframework.transaction.annotation.EnableTransactionManagement
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.receiver.ReceiverPartition
import reactor.kafka.sender.SenderOptions
import java.util.*


@Configuration
@EnableTransactionManagement
class PickTicketReportsConfig {

    private val log = LoggerFactory.getLogger(PickTicketReportsConfig::class.java)

    /**
     * Kafka Configuration
     */

    @Autowired
    private lateinit var env: Environment

    /**
     * Reactor Kafka Configuration
     */
    @Bean
    fun kafkaReceiver(): KafkaReceiver<String, String> {

        val props: MutableMap<String, Any> = mutableMapOf()
        env.getProperty("kafka.bootstrap.servers")?.let { props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, it) }
        env.getProperty("kafka.consumer.client-id")?.let { props.put(ConsumerConfig.CLIENT_ID_CONFIG, it) }
        env.getProperty("kafka.consumer.group-id")?.let { props.put(ConsumerConfig.GROUP_ID_CONFIG, it) }
        props[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
        props[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
        env.getProperty("kafka.consumer.auto-offset-reset")?.let { props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, it) }
        val receiverOptions = ReceiverOptions.create<String, String>(props)

        receiverOptions.subscription(Collections.singleton(env.getProperty("kafka.topic.webflux")))

        return KafkaReceiver.create(receiverOptions)
    }

//    @Bean
//    fun kafkaReceiverOptions(topic: String?, kafkaProperties: KafkaProperties): ReceiverOptions<String, String> {
//        val basicReceiverOptions = ReceiverOptions.create<String, String>(kafkaProperties.buildConsumerProperties())
//        return basicReceiverOptions.subscription(Collections.singletonList(topic))
//            .addAssignListener { receiverPartitions: Collection<ReceiverPartition?>? ->
//                log.debug(
//                    "onPartitionAssigned {}",
//                    receiverPartitions
//                )
//            }
//            .addRevokeListener { receiverPartitions: Collection<ReceiverPartition?>? ->
//                log.debug(
//                    "onPartitionsRevoked {}",
//                    receiverPartitions
//                )
//            }
//    }
//
//    @Bean
//    fun kafkaConsumerTemplate(kafkaReceiverOptions: ReceiverOptions<String, String>?): ReactiveKafkaConsumerTemplate<String, String> {
//        return ReactiveKafkaConsumerTemplate(kafkaReceiverOptions!!)
//    }

//    @Bean
//    fun kafkaProducerTemplate(
//        properties: KafkaProperties
//    ): ReactiveKafkaProducerTemplate<String, List<Any>> {
//        val props = properties.buildProducerProperties()
//        return ReactiveKafkaProducerTemplate(SenderOptions.create(props))
//    }
}
