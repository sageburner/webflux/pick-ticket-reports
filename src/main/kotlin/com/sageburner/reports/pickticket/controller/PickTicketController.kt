package com.sageburner.reports.pickticket.controller

import com.sageburner.reports.pickticket.model.PickTicketEventCount
import reactor.core.publisher.Mono


interface PickTicketController {
    fun getCounts(): Mono<List<PickTicketEventCount>>
}
