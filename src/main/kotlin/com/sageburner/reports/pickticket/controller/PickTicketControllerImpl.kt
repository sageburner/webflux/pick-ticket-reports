package com.sageburner.reports.pickticket.controller

import com.sageburner.reports.pickticket.model.PickTicketEventCount
import com.sageburner.reports.pickticket.service.PickTicketEventService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono


@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping(value = ["/pickticketevent"])
class PickTicketControllerImpl : PickTicketController {

    private val log = LoggerFactory.getLogger(PickTicketControllerImpl::class.java)

    @Autowired
    private lateinit var pickTicketEventService: PickTicketEventService

    @GetMapping("/counts")
    override fun getCounts(): Mono<List<PickTicketEventCount>> {
        return pickTicketEventService.getCounts()
    }
}
