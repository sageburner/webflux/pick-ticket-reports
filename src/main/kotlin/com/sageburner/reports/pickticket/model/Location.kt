package com.sageburner.reports.pickticket.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Location
 *
 */
data class Location(
        @JsonProperty("locationId") val id: String,
        @JsonProperty("locationName") val name: String,
        @get:JsonIgnore val isSupplying: Boolean)
