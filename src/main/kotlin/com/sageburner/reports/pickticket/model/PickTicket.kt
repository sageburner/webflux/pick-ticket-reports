package com.sageburner.reports.pickticket.model

import java.util.*

/**
 * {
 *   "id" : "3f6e266b-3a8b-4abd-bdeb-0eec4c75db1e",
 *   "supplyingLocation" : {
 *     "locationId" : "5090ba02-c626-4186-a8c6-995e41d3a86e",
 *     "locationName" : "Central Supply"
 *   },
 *   "requestingLocation" : {
 *     "locationId" : "b57857d5-76d7-427b-9d44-bd953b96aec3",
 *     "locationName" : "ER"
 *   },
 *   "note" : "The greatest barrier to success is the fear of failure.",
 *   "caseNumber" : "15584",
 *   "requisitionLines" : [ {
 *     "id" : "c988f33b-9c01-4789-a08f-b8b64c94b27c",
 *     "qtyNeeded" : "4",
 *     "parId" : "1cc0ff70-58d7-4926-a077-e309c63f8d5f"
 *   } ]
 * }
*/
data class PickTicket(
        val id: UUID,
        val supplyingLocation: Location,
        val requestingLocation: Location,
        val note: String = "note",
        val caseNumber: String?,
        val requisitionLines: List<RequisitionLine>)
