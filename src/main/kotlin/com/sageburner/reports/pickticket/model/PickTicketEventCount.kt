package com.sageburner.reports.pickticket.model

import java.math.BigInteger

data class PickTicketEventCount(
        val eventName: String,
        val count: BigInteger)
