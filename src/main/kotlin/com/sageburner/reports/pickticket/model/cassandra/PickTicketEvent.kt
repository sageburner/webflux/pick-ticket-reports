package com.sageburner.reports.pickticket.model.cassandra

import com.sageburner.reports.pickticket.model.PickTicket
import org.springframework.data.annotation.Transient
import org.springframework.data.cassandra.core.cql.Ordering
import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import org.springframework.data.cassandra.core.mapping.Table
import java.time.OffsetDateTime
import java.util.*

@Table
data class PickTicketEvent(
        @PrimaryKeyColumn(
                name = "id",
                ordinal = 1,
                type = PrimaryKeyType.CLUSTERED,
                ordering = Ordering.ASCENDING)
        val id: UUID = UUID.randomUUID(),
        @PrimaryKeyColumn(
                name = "type",
                ordinal = 0,
                type = PrimaryKeyType.PARTITIONED)
        val type: PickTicketEventType,
        val timestamp: String = OffsetDateTime.now().toString(),
        @Transient
        val pickTicket: PickTicket)

enum class PickTicketEventType {
    CREATED, MODIFIED
}
