package com.sageburner.reports.pickticket.repository.cassandra

import com.sageburner.reports.pickticket.model.cassandra.PickTicketEvent
import org.springframework.data.cassandra.repository.CassandraRepository
import java.util.*

interface PickTicketEventRepository : CassandraRepository<PickTicketEvent, UUID>