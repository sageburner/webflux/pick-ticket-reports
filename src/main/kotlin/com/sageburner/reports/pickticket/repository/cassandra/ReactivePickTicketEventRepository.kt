package com.sageburner.reports.pickticket.repository.cassandra

import com.sageburner.reports.pickticket.model.PickTicketEventCount
import com.sageburner.reports.pickticket.model.cassandra.PickTicketEventType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.cassandra.core.ReactiveCassandraOperations
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigInteger

@Component
class ReactivePickTicketEventRepository {
    @Autowired
    private lateinit var reactiveCassandraOperations: ReactiveCassandraOperations

    fun getCounts(): Mono<List<PickTicketEventCount>> {
        val createdCql = "select count(*) from webflux.pickticketevent where type = 'CREATED'"
        val modifiedCql = "select count(*) from webflux.pickticketevent where type = 'MODIFIED'"

        val createdMono = reactiveCassandraOperations.reactiveCqlOperations.queryForObject(createdCql, BigInteger::class.java).map {
            count ->
            PickTicketEventCount(PickTicketEventType.CREATED.name, count)
        }

        val modifiedMono = reactiveCassandraOperations.reactiveCqlOperations.queryForObject(modifiedCql, BigInteger::class.java).map {
            count ->
            PickTicketEventCount(PickTicketEventType.MODIFIED.name, count)
        }

        return Flux.concat(createdMono, modifiedMono).collectList()
    }
}
