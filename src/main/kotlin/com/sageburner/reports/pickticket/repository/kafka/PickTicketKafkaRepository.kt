package com.sageburner.reports.pickticket.repository.kafka

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverRecord


/**
 * PickTicketKafkaRepository
 *
 */
@Component
class PickTicketKafkaRepository {
    private val log = LoggerFactory.getLogger(PickTicketKafkaRepository::class.java)

    @Autowired
    private lateinit var kafkaReceiver: KafkaReceiver<String, String>

    fun receieveRx(): Flux<ReceiverRecord<String, String>> {
        return kafkaReceiver.receive()
    }
}
