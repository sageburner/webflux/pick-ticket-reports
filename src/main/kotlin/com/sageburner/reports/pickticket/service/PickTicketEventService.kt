package com.sageburner.reports.pickticket.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.sageburner.reports.pickticket.model.PickTicketEventCount
import com.sageburner.reports.pickticket.model.cassandra.PickTicketEvent
import com.sageburner.reports.pickticket.repository.cassandra.PickTicketEventRepository
import com.sageburner.reports.pickticket.repository.cassandra.ReactivePickTicketEventRepository
import com.sageburner.reports.pickticket.repository.kafka.PickTicketKafkaRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverRecord
import java.text.SimpleDateFormat
import java.util.*

@Service
class PickTicketEventService : InitializingBean, DisposableBean {

    private val log = LoggerFactory.getLogger(PickTicketEventService::class.java)

    @Autowired
    private lateinit var pickTicketKafkaRepository: PickTicketKafkaRepository

    @Autowired
    private lateinit var pickTicketEventRepository: PickTicketEventRepository

    @Autowired
    private lateinit var reactivePickTicketEventRepository: ReactivePickTicketEventRepository

    private val mapper = jacksonObjectMapper()

    val dateFormat = SimpleDateFormat("HH:mm:ss:SSS z dd MMM yyyy")

    override fun afterPropertiesSet() {
        log.info("PickTicketEventService Initialized...")

        pickTicketKafkaRepository.receieveRx().subscribe { record ->
            val offset = record.receiverOffset()
            offset.acknowledge()

            val pickTicketEvent = fromJson(record.value())
            log.info("PickTicketEvent: $pickTicketEvent")
            pickTicketEventRepository.save(pickTicketEvent)
        }
    }

    override fun destroy() {
        log.info("Destroying ...")
    }

    private fun logVerbose(record: ReceiverRecord<Any, Any>) {
        val offset = record.receiverOffset()
        log.info(
                String.format(
                        "Received message: topic-partition=%s offset=%d timestamp=%s key=%s value=%s\n",
                        offset.topicPartition().toString(),
                        offset.offset(),
                        dateFormat.format(Date(record.timestamp())),
                        record.key(),
                        record.value()
                )
        )
    }

    private fun fromJson(input: String): PickTicketEvent {
        return mapper.readValue(input, PickTicketEvent::class.java)
    }

    fun getCounts(): Mono<List<PickTicketEventCount>> {
        return reactivePickTicketEventRepository.getCounts()
    }
}
